numpy>=1.11.0
scipy>=0.17.0
tinyarray>=1.2

[continuum]
sympy>=0.7.6

[plotting]
matplotlib>=1.5.1

[qsymm]
qsymm>=1.2.6
